Source: speech-dispatcher
Section: sound
Priority: optional
Maintainer: Debian TTS Team <tts-project@lists.alioth.debian.org>
Uploaders:
 Paul Gevers <elbrus@debian.org>
Build-Depends:
 automake,
 autotools-dev,
 debhelper (>= 9),
 dh-autoreconf,
 flite1-dev (>= 1.4),
 intltool,
 libao-dev,
 libasound2-dev [linux-any],
 libaudio-dev,
 libdotconf-dev (>= 1.3),
 libespeak-dev,
 libglib2.0-dev,
 libltdl-dev,
 libpulse-dev,
 libtool,
 libxau-dev,
 python3,
 texinfo
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=tts/speech-dispatcher.git;a=summary
Vcs-Git: git://anonscm.debian.org/tts/speech-dispatcher.git
Homepage: http://devel.freebsoft.org/speechd
X-Python-Version: >= 3.0
Standards-Version: 3.9.5

Package: speech-dispatcher
Architecture: any
Depends:
 adduser,
 lsb-base (>= 3.0-10),
 speech-dispatcher-audio-plugins (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Recommends:
 pulseaudio
Suggests:
 libttspico-utils,
 speech-dispatcher-doc-cs,
 speech-dispatcher-festival
Description: Common interface to speech synthesizers
 Speech Dispatcher provides a device independent layer for speech synthesis.
 It supports various software and hardware speech synthesizers as
 backends and provides a generic layer for synthesizing speech and
 playing back PCM data via those different backends to applications.
 .
 Various high level concepts like enqueueing vs. interrupting speech and
 application specific user configurations are implemented in a device
 independent way, therefore freeing the application programmer from
 having to yet again reinvent the wheel.
 .
 This package contains Speech Dispatcher itself.

Package: libspeechd2
Architecture: any
Multi-arch: same
Section: libs
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Pre-Depends:
 multiarch-support,
 ${misc:Pre-Depends}
Description: Speech Dispatcher: Shared libraries
 Speech Dispatcher provides a device independent layer for speech synthesis.
 It supports various software and hardware speech synthesizers as
 backends and provides a generic layer for synthesizing speech and
 playing back PCM data via those different backends to applications.
 .
 Various high level concepts like enqueueing vs. interrupting speech and
 application specific user configurations are implemented in a device
 independent way, therefore freeing the application programmer from
 having to yet again reinvent the wheel.
 .
 This package contains a shared library needed for C programs linked with it.

Package: libspeechd-dev
Architecture: any
Section: libdevel
Priority: extra
Depends:
 libspeechd2 (= ${binary:Version}),
 ${misc:Depends}
Suggests:
 speech-dispatcher
Description: Speech Dispatcher: Development libraries and header files
 Speech Dispatcher provides a device independent layer for speech synthesis.
 It supports various software and hardware speech synthesizers as
 backends and provides a generic layer for synthesizing speech and
 playing back PCM data via those different backends to applications.
 .
 Various high level concepts like enqueueing vs. interrupting speech and
 application specific user configurations are implemented in a device
 independent way, therefore freeing the application programmer from
 having to yet again reinvent the wheel.
 .
 This package contains static library, and C headers needed for programs
 linked with the shared or static library.

Package: cl-speech-dispatcher
Architecture: all
Section: lisp
Priority: extra
Depends:
 cl-regex,
 common-lisp-controller,
 ${misc:Depends}
Description: Common Lisp interface to Speech Dispatcher
 Speech Dispatcher provides a device independent layer for speech synthesis.
 It supports various software and hardware speech synthesizers as
 backends and provides a generic layer for synthesizing speech and
 playing back PCM data via those different backends to applications.
 .
 Various high level concepts like enqueueing vs. interrupting speech and
 application specific user configurations are implemented in a device
 independent way, therefore freeing the application programmer from
 having to yet again reinvent the wheel.
 .
 This package contains a Common Lisp library for communication with Speech
 Dispatcher.

Package: python3-speechd
Architecture: all
Section: python
Priority: extra
Breaks:
 python-speechd
Replaces:
 python-speechd
Depends:
 python3-xdg,
 ${misc:Depends},
 ${python3:Depends}
Description: Python interface to Speech Dispatcher
 Speech Dispatcher provides a device independent layer for speech synthesis.
 It supports various software and hardware speech synthesizers as
 backends and provides a generic layer for synthesizing speech and
 playing back PCM data via those different backends to applications.
 .
 Various high level concepts like enqueueing vs. interrupting speech and
 application specific user configurations are implemented in a device
 independent way, therefore freeing the application programmer from
 having to yet again reinvent the wheel.
 .
 This package contains a Python library for communication with Speech
 Dispatcher.

Package: speech-dispatcher-festival
Architecture: any
Depends:
 festival,
 festival-freebsoft-utils (>= 0.6),
 speech-dispatcher (>= 0.6),
 ${misc:Depends},
 ${shlibs:Depends}
Breaks:
 speech-dispatcher (<< 0.8)
Replaces:
 speech-dispatcher (<< 0.8)
Recommends:
 sound-icons
Description: Festival support for Speech Dispatcher
 Speech Dispatcher provides a device independent layer for speech synthesis.
 It supports various software and hardware speech synthesizers as
 backends and provides a generic layer for synthesizing speech and
 playing back PCM data via those different backends to applications.
 .
 Various high level concepts like enqueueing vs. interrupting speech and
 application specific user configurations are implemented in a device
 independent way, therefore freeing the application programmer from
 having to yet again reinvent the wheel.
 .
 This package contains dependencies on packages necessary for running Speech
 Dispatcher with Festival.

Package: speech-dispatcher-doc-cs
Architecture: all
Section: doc
Priority: extra
Depends:
 ${misc:Depends}
Suggests:
 speech-dispatcher
Description: Speech Dispatcher documentation in Czech
 Speech Dispatcher provides a device independent layer for speech synthesis.
 It supports various software and hardware speech synthesizers as
 backends and provides a generic layer for synthesizing speech and
 playing back PCM data via those different backends to applications.
 .
 Various high level concepts like enqueueing vs. interrupting speech and
 application specific user configurations are implemented in a device
 independent way, therefore freeing the application programmer from
 having to yet again reinvent the wheel.
 .
 This package contains Czech documentation of Speech Dispatcher.

Package: speech-dispatcher-dbg
Architecture: any
Section: debug
Priority: extra
Depends:
 libspeechd2 (= ${binary:Version}),
 speech-dispatcher (= ${binary:Version}),
 speech-dispatcher-festival (= ${binary:Version}),
 ${misc:Depends}
Description: Speech Dispatcher debugging symbols
 Speech Dispatcher provides a device independent layer for speech synthesis.
 It supports various software and hardware speech synthesizers as
 backends and provides a generic layer for synthesizing speech and
 playing back PCM data via those different backends to applications.
 .
 Various high level concepts like enqueueing vs. interrupting speech and
 application specific user configurations are implemented in a device
 independent way, therefore freeing the application programmer from
 having to yet again reinvent the wheel.
 .
 This package contains the debugging symbols for Speech Dispatcher.

Package: speech-dispatcher-audio-plugins
Architecture: any
Multi-arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Pre-Depends:
 multiarch-support
Breaks:
 speech-dispatcher (<< 0.8-1)
Replaces:
 speech-dispatcher (<< 0.8-1)
Description: Speech Dispatcher: Audio output plugins
 Speech Dispatcher provides a device independent layer for speech synthesis.
 It supports various software and hardware speech synthesizers as
 backends and provides a generic layer for synthesizing speech and
 playing back PCM data via those different backends to applications.
 .
 Various high level concepts like enqueueing vs. interrupting speech and
 application specific user configurations are implemented in a device
 independent way, therefore freeing the application programmer from
 having to yet again reinvent the wheel.
 .
 This package contains all the speech-dispatcher audio output plugins.
